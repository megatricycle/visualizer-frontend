import React from 'react';

import styles from './Intro.scss';

class Intro extends React.Component {
    render(){
        return (
            <div className={styles.container}>
                <p className={styles.welcome}>Welcome to the Freedom! Visualizer.</p>
                <p className={styles.instructions}>To start, select a song from the left menu.</p>
            </div>
        );
    }
}


export default Intro;