import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/controlPanelActions/controlPanelActions';
import ControlPanel from '../../components/ControlPanel/ControlPanel';
import styles from './ControlPanelPage.scss';

class ControlPanelPage extends React.Component {
    componentWillMount() {
        if (this.props.controlPanel.track_id === -1) {
            this.props.history.push('/');
        }
    }

    render(){
        return (
            <div className={styles.wrapper}>
                <ControlPanel
                    bg_image={this.props.controlPanel.bg_image}
                    v_mode={this.props.controlPanel.v_mode}
                    genres={this.props.controlPanel.genres}
                    changeBackground={this.props.actions.changeBackground}
                    changeVMode={this.props.actions.changeVMode}
                    changeTitle={this.props.actions.changeTitle}
                    changeArtist={this.props.actions.changeArtist}
                    toggleGenre={this.props.actions.toggleGenre}
                />
                <div className={styles.bottomContainer}>
                    <div>
                        <p className={styles.instruction}>
                            Upload an image.
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}


ControlPanelPage.propTypes = {
    actions: PropTypes.object.isRequired,
    controlPanel: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        controlPanel: state.controlPanel
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ControlPanelPage);