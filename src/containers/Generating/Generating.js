import React from 'react';
import FontIcon from 'material-ui/FontIcon';

import styles from './Generating.scss';

class Generating extends React.Component {
    render(){
        return (
            <div className={styles.container}>
                <p>
                    <FontIcon 
                        className={'material-icons ' + styles.smile}
                    >
                        build
                    </FontIcon>
                </p>
                <p className={styles.generating}>Generating your video...</p>
                <p className={styles.subText}>This is gonna take a while! Wait about 5 minutes. Do not close your browser, or else you won't be able to download the video!</p>
            </div>
        );
    }
}


export default Generating;