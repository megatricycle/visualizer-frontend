import React from 'react';
import FontIcon from 'material-ui/FontIcon';

import styles from './Success.scss';

class Success extends React.Component {
    render(){
        return (
            <div className={styles.container}>
                <p>
                    <FontIcon 
                        className={'material-icons ' + styles.smile}
                    >
                        insert_emoticon
                    </FontIcon>
                </p>
                <p className={styles.welcome}>Success!</p>
                <p className={styles.instructions}>Your video should automatically be downloaded. How about creating another one?</p>
            </div>
        );
    }
}


export default Success;