import * as actions from '../../actions/controlPanelActions/controlPanelActionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

export default function controlPanelReducer(state = initialState.controlPanel, action) {
    let toggled = {
        genres: {}
    };

    switch (action.type) {
        case actions.CHANGE_BACKGROUND:
            return objectAssign({}, state, {
                bg_image: action.bg_image
            });

        case actions.CHANGE_V_MODE:
            return objectAssign({}, state, {
                v_mode: action.v_mode
            });

        case actions.TOGGLE_GENRE:
            toggled.genres[action.genre] = !state.genres[action.genre];

            return objectAssign({}, state, {
                genres: objectAssign({}, state.genres, toggled.genres)
            });

        case actions.CHANGE_TITLE:
            return objectAssign({}, state, {
                title: action.title
            });

        case actions.CHANGE_ARTIST:
            return objectAssign({}, state, {
                artist: action.artist
            });

        case actions.SELECT_TRACK:
            return objectAssign({}, state, {
                track_id: action.track_id,
                file_name: action.file_name
            });
            
        case actions.RESET_STATE:
            return objectAssign({}, initialState.controlPanel, {});

        default:
            return state;
    }
}
