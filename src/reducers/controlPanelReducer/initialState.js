export default {
    controlPanel: {
        track_id: -1,
        file_name: '',
        title: '',
        artist: '',
        bg_image: '',
        v_mode: '',
        genres: {
            rock: false,
            classic: false,
            rnb: false,
            acoustic: false
        }
    }
};
