// Set up your root reducer here...
 import { combineReducers } from 'redux';

import controlPanel from './controlPanelReducer/controlPanelReducer';
import app from './appReducer/appReducer';

const rootReducer = combineReducers({
    controlPanel,
    app
});

 export default rootReducer;