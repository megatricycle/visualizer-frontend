import * as actions from '../../actions/appActions/appActionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

export default function appReducer(state = initialState.app, action) {
    switch (action.type) {
        case actions.ADD_TRACKS:
            return objectAssign({}, state, {
                tracks: [...state.tracks, ...action.tracks],
                listIndex: state.listIndex + 50
            });

        case actions.PLAY_MUSIC:
            return objectAssign({}, state, {
                track_playing: action.track_id
            });

        case actions.STOP_MUSIC:
            return objectAssign({}, state, {
                track_playing: ''
            });

        case actions.LIST_LOADED:
            return objectAssign({}, state, {
                listIsLoading: false
            });

        case actions.LIST_LOAD_START:
            return objectAssign({}, state, {
                listIsLoading: true
            });

        default:
            return state;
    }
}
