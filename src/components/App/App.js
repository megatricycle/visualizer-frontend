import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import FlatButton from 'material-ui/FlatButton';
import ContentSend from 'material-ui/svg-icons/content/send';
import { grey900, deepOrange900 } from 'material-ui/styles/colors';

import * as appActions from '../../actions/appActions/appActions';
import * as controlPanelActions from '../../actions/controlPanelActions/controlPanelActions';
import Search from '../Search/Search';
import ListOfTracks from '../ListOfTracks/ListOfTracks';
import styles from './App.scss';

class App extends React.Component {
    constructor() {
        super();

        this.generate = this.generate.bind(this);
    }

    componentWillMount() {
        this.controlPanelReset();
        this.getTracks();
    }

    controlPanelReset() {
        this.props.controlPanelActions.reset();
    }

    generate() {
        const params = {
            title: this.props.controlPanel.title,
            artist: this.props.controlPanel.artist,
            file_name: this.props.controlPanel.file_name,
            bg_filename: this.props.controlPanel.bg_image,
            rock: this.props.controlPanel.genres.rock,
            rnb: this.props.controlPanel.genres.rnb,
            classic: this.props.controlPanel.genres.classic,
            acoustic: this.props.controlPanel.genres.acoustic,
            v_mode: this.props.controlPanel.v_mode
        };

        const url = 'http://localhost:8000/track/generate';

        const data = new FormData();
        data.append('json', JSON.stringify(params));

        this.props.history.push('/generating');
        this.props.controlPanelActions.reset();

        fetch(url, {
            method: 'POST',
            body: data
        })
            .then((res) => {
                if(res.ok) {
                    return res.json();
                }
                else {
                    return Promise.reject(res);
                }
            })
            .then((res) => {
                this.props.history.push('/success');

                setTimeout(() => {
                    window.open('http://localhost:8000' + res.data.url);
                }, 3000);
            })
            .catch((err) => console.error(err));
    }

    getTracks() {
        const index = this.props.app.listIndex;

        this.props.appActions.listLoadStart();

        fetch('http://localhost:8000/track?index=' + index, {
            method: 'GET'
        })
        .then((res) =>  res.json())
        .then((tracks) => {
            this.props.appActions.addTracks(tracks.data);
            this.props.appActions.listLoaded();
        })
        .catch(() => {
            // @TODO: error handling
        });
    }

    render() {
        const generateElement = (
            <FlatButton
                label="Generate"
                labelPosition="before"
                className={styles.generate}
                icon={
                    <ContentSend color="white"/>
                }
                onClick={this.generate}
            />
        );

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
                <div>
                    <AppBar
                        title={
                            <Link to="/" className={styles.logo} onClick={() => this.controlPanelReset()}>
                                Freedom! Visualizer
                            </Link>
                        }
                        showMenuIconButton={false}
                        iconElementRight={
                            this.props.location.pathname === '/step-two' ?
                            generateElement :
                            <div></div>
                        }
                        iconStyleRight={{
                            marginRight: '0px'
                        }}
                        style={{
                            zIndex: '1301',
                            background: grey900 
                        }}
                    />
                    <Drawer
                        open={true}
                        width={300}
                        containerStyle={{
                            paddingTop: '64px'
                        }}
                    >
                        <div
                            className={styles.searchContainer}
                            style={{
                                background: deepOrange900
                            }}
                        >
                            <Search/>
                        </div>

                        <div className={styles.tracksContainer}>
                            <ListOfTracks
                                tracks={this.props.app.tracks}
                                track_playing={this.props.app.track_playing}
                                track_selected={this.props.controlPanel.track_id}
                                playMusic={this.props.appActions.playMusic}
                                stopMusic={this.props.appActions.stopMusic}
                                selectTrack={this.props.controlPanelActions.selectTrack}
                                listLoaded={this.props.appActions.listLoaded}
                                isLoading={this.props.app.listIsLoading}
                                loadMore={() => this.getTracks()}
                            />
                        </div>                    
                    </Drawer>
                    <div className={styles.content}>
                        {this.props.children}
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

App.propTypes = {
    app: PropTypes.object.isRequired,
    appActions: PropTypes.object.isRequired,
    controlPanel: PropTypes.object.isRequired,
    controlPanelActions: PropTypes.object.isRequired,
    children: PropTypes.element.isRequired,
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        app: state.app,
        controlPanel: state.controlPanel
    };
}

function mapDispatchToProps(dispatch) {
    return {
        appActions: bindActionCreators(appActions, dispatch),
        controlPanelActions: bindActionCreators(controlPanelActions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);