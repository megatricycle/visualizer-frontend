import React, { PropTypes } from 'react';
import ScrollArea from 'react-scrollbar';
import { List} from 'material-ui/List';
import { grey400 } from 'material-ui/styles/colors';

import Track from '../Track/Track';

const styles = require('./ListOfTracks.scss');
const INFINITE_SCROLL_OFFSET = 50;

class ListOfTracks extends React.Component {
    playMusic(track_id, file_path) {
        this.props.playMusic(track_id);

        this.refs.player.src = 'http://d1gyn9a1opa35u.cloudfront.net/tracks/' + file_path;
        this.refs.player.play();
    }

    stopMusic() {
        this.props.stopMusic();
        this.refs.player.pause();
        this.refs.player.currentTime = 0;
    }

    selectTrack(track_id, file_name) {
        this.props.selectTrack(track_id, file_name);
    }

    handleScroll(value) {
        if(value.topPosition + value.containerHeight > value.realHeight - INFINITE_SCROLL_OFFSET && !this.props.isLoading) {
            this.loadMore();
        }
    }

    loadMore() {
        this.props.loadMore();
    }

    componentDidMount() {
        this.props.listLoaded();
    }

    render() {
        return (
            <ScrollArea
                speed={0.8}
                horizontal={false}
                className={styles.scrollArea}
                onScroll={(value) => {this.handleScroll(value);}}                
                style={{
                    background: grey400
                }}
            >
                <audio ref="player"></audio>
                <List>
                    {this.props.tracks.map((track) => (
                        <Track
                            key={track.track_id}
                            track={track}
                            play={() => this.playMusic(track.track_id, track.file_path)}
                            stop={() => this.stopMusic()}
                            select={() => this.selectTrack(track.track_id, track.file_path)}
                            is_playing={this.props.track_playing === track.track_id}
                            is_selected={this.props.track_selected === track.track_id}
                        />
                    ))}
                </List>
            </ScrollArea>
        );
    }
}

ListOfTracks.propTypes = {
    tracks: PropTypes.array.isRequired,
    track_playing: PropTypes.string.isRequired,
    track_selected: PropTypes.number.isRequired,
    playMusic: PropTypes.func.isRequired,
    stopMusic: PropTypes.func.isRequired,
    selectTrack: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    listLoaded: PropTypes.func.isRequired,
    loadMore: PropTypes.func.isRequired
};

export default ListOfTracks;