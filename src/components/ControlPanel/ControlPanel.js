import React, { PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import ImageLinkedCamera from 'material-ui/svg-icons/image/linked-camera';
import AVEqualizer from 'material-ui/svg-icons/av/equalizer';
import AVAlbum from 'material-ui/svg-icons/av/album';
import ImageMusicNote from 'material-ui/svg-icons/image/music-note';
import ActionPanTool from 'material-ui/svg-icons/action/pan-tool';
import AVHearing from 'material-ui/svg-icons/av/hearing';
import ActionStars from 'material-ui/svg-icons/action/stars';

import styles from './ControlPanel.scss';

const V_ICON_SIZE = 175;
const GENRE_ICON_SIZE = 75;

const localStyles = {
    vIconStyle: {
        width: V_ICON_SIZE,
        height: V_ICON_SIZE
    },
    vIconButton: {
        width: V_ICON_SIZE * 2,
        height: V_ICON_SIZE * 2,
        padding: 30
    },
    genreIconStyle: {
        width: GENRE_ICON_SIZE,
        height: GENRE_ICON_SIZE
    },
    genreIconButton: {
        width: GENRE_ICON_SIZE * 1.5,
        height: GENRE_ICON_SIZE * 1.5,
        padding: 15
    }
};

class ControlPanel extends React.Component {
    constructor() {
        super();

        this.handleUpload = this.handleUpload.bind(this);
    }

    handleUpload() {
        this.refs.uploader.click();
    }

    onChangeUploader() {
        let file = this.refs.uploader.files[0];
        let reader = new FileReader();
        
        reader.addEventListener('load', () => {
            this.props.changeBackground(reader.result);
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    }

    changeVMode(v_mode) {
        this.props.changeVMode(v_mode);
    }

    toggleGenre(genre) {
        this.props.toggleGenre(genre);
    }

    changeTitle(title) {
        this.props.changeTitle(title);
    }

    changeArtist(artist) {
        this.props.changeArtist(artist);
    }

    render() {
        return (
            <div 
                className={styles.wrapper}
                style={
                    this.props.bg_image ?
                    {
                        backgroundImage: 'url(' + this.props.bg_image + ')'
                    } :
                    {
                        backgroundColor: 'black'
                    }
                }
            >
                <div className={styles.overlay}></div>
                <div className={styles.flexWrapper}>
                    <div className={styles.partTop}>
                        <div className={styles.padTop}>
                            <FlatButton
                                label="Upload Background"
                                labelPosition="before"
                                icon={<ImageLinkedCamera/>}
                                style={{
                                    opacity: 0.4
                                }}
                                onClick={this.handleUpload}
                            />
                            <input
                                id="uploader"
                                ref="uploader"
                                type="file"
                                accept="image/*"
                                onChange={() => this.onChangeUploader()}
                                style={{display: 'none'}}
                            />
                        </div>
                    </div>
                    <div className={styles.partMid}>
                        <p>
                            <IconButton
                                iconStyle={Object.assign({}, localStyles.vIconStyle, {
                                    opacity: this.props.v_mode === 'bar' ? 1 : 0.2
                                })}
                                style={localStyles.vIconButton}
                                onClick={() => this.changeVMode('bar')}
                            >
                                <AVEqualizer className={styles.vIcon}/>
                            </IconButton>
                        </p>
                        <p>
                            <IconButton
                                iconStyle={Object.assign({}, localStyles.vIconStyle, {
                                    opacity: this.props.v_mode === 'pulse' ? 1 : 0.2
                                })}
                                style={localStyles.vIconButton}
                                onClick={() => this.changeVMode('pulse')}
                            >
                                <AVAlbum className={styles.vIcon}/>
                            </IconButton>
                        </p>
                    </div>
                    <div className={styles.partBot}>
                        <div className={styles.textInputContainer}>
                            <TextField
                                hintText="Title"
                                underlineShow={false}
                                style={{
                                    fontSize: '3em',
                                    width: '100%'
                                }}
                                inputStyle={{
                                    color: 'white',
                                    opacity: 0.7,
                                    height: '100% !important'
                                }}
                                onChange={(dispatch, value) => {
                                    this.changeTitle(value);
                                }}
                            />
                            <TextField
                                hintText="Artist"
                                underlineShow={false}
                                style={{
                                    fontSize: '1.5em',
                                    width: '100%'
                                }}
                                inputStyle={{
                                    color: 'white',
                                    opacity: 0.4
                                }}
                                onChange={(dispatch, value) => {
                                    this.changeArtist(value);
                                }}
                            />
                        </div>
                        <div className={styles.genreContainer}>
                            <IconButton
                                iconStyle={Object.assign({}, localStyles.genreIconStyle, {
                                    opacity: this.props.genres.rock ? 1 : 0.2
                                })}
                                onClick={() => this.toggleGenre('rock')}
                                style={localStyles.genreIconButton}
                            >
                                <ActionPanTool className={styles.vIcon}/>
                            </IconButton>
                            <IconButton
                                iconStyle={Object.assign({}, localStyles.genreIconStyle, {
                                    opacity: this.props.genres.classic ? 1 : 0.2
                                })}
                                onClick={() => this.toggleGenre('classic')}
                                style={localStyles.genreIconButton}
                            >
                                <ActionStars className={styles.vIcon}/>
                            </IconButton>
                            <IconButton
                                iconStyle={Object.assign({}, localStyles.genreIconStyle, {
                                    opacity: this.props.genres.acoustic ? 1 : 0.2
                                })}
                                onClick={() => this.toggleGenre('acoustic')}
                                style={localStyles.genreIconButton}
                            >
                                <AVHearing className={styles.vIcon}/>
                            </IconButton>
                            <IconButton
                                iconStyle={Object.assign({}, localStyles.genreIconStyle, {
                                    opacity: this.props.genres.rnb ? 1 : 0.2
                                })}
                                onClick={() => this.toggleGenre('rnb')}
                                style={localStyles.genreIconButton}
                            >
                                <ImageMusicNote className={styles.vIcon}/>
                            </IconButton>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ControlPanel.propTypes = {
    bg_image: PropTypes.string.isRequired,
    v_mode: PropTypes.string.isRequired,
    genres: PropTypes.object.isRequired,
    changeBackground: PropTypes.func.isRequired,
    changeVMode: PropTypes.func.isRequired,
    toggleGenre: PropTypes.func.isRequired,
    changeTitle: PropTypes.func.isRequired,
    changeArtist: PropTypes.func.isRequired
};

export default ControlPanel;