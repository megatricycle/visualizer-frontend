import React from 'react';
import TextField from 'material-ui/TextField';
import FontIcon from 'material-ui/FontIcon';
import { deepOrangeA100 } from 'material-ui/styles/colors';

const styles = require('./Search.scss');

class Search extends React.Component {
    render() {
        return (
            <div
                className={styles.searchContainer}
                
            >
                <img src="http://www.tunes.tm/images/t-logo-with-beats.svg"/>

                <a href="http://www.tunes.tm" target="_blank" className={styles.noUnderline}>
                    <p className={styles.tunesTM}>Tunes.tm</p>
                </a>
                {/*<TextField
                    hintText={
                        <span>
                            <FontIcon
                                className="material-icons"
                                style={{
                                    top: '0.25em',
                                    opacity: '0.298039'
                                }}
                            >
                                search
                            </FontIcon> Search
                        </span>
                    }
                    underlineFocusStyle={{
                        borderColor: deepOrangeA100
                    }}
                />*/}
            </div>
        );
    }
}

export default Search;