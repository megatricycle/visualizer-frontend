import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import HardwareHeadset from 'material-ui/svg-icons/hardware/headset';
import AVPlay from 'material-ui/svg-icons/av/play-arrow';
import AVStop from 'material-ui/svg-icons/av/stop';
import { deepOrange900, deepOrangeA200 } from 'material-ui/styles/colors';

const styles = require('./Track.scss');

class Track extends React.Component {
    handleClickPlay() {
        this.props.play();
    }

    handleClickStop() {
        this.props.stop();
    }

    handleClickSelect() {
        this.props.select();
    }

    render() {
        return (
            <Link to="/step-two" className={styles.noDecoration} onClick={() => this.handleClickSelect()}>
                <ListItem
                    leftAvatar={
                        this.props.track.picture ?
                        <Avatar src={this.props.track.picture}/> :
                        <Avatar
                            icon={<HardwareHeadset/>}
                            color="white"
                            backgroundColor={deepOrange900}
                        />
                    }
                    rightAvatar={
                        this.props.is_playing ?
                        <Avatar className={styles.play} 
                            icon={<AVStop />} 
                            onClick={() => this.handleClickStop()}
                        /> :
                        <Avatar className={styles.play} 
                            icon={<AVPlay />} 
                            onClick={() => this.handleClickPlay()}
                        />
                        
                    }
                    primaryText={<span className={styles.title}>{this.props.track.title}</span>}
                    secondaryText={this.props.track.name}
                    track_id={this.props.track.track_id}
                    style={{
                        backgroundColor: this.props.is_selected ? deepOrangeA200: '',
                        transition: 'all 0.4s'
                    }}
                />
            </Link>
        );
    }
}

Track.propTypes = {
    track: PropTypes.object.isRequired,
    play: PropTypes.func.isRequired,
    stop: PropTypes.func.isRequired,
    select: PropTypes.func.isRequired,
    is_playing: PropTypes.bool.isRequired,
    is_selected: PropTypes.bool.isRequired
};

export default Track;