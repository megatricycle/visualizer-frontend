import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App/App';
import Intro from './containers/Intro/Intro';
import ControlPanel from './containers/ControlPanel/ControlPanelPage';
import Success from './containers/Success/Success';
import Generating from './containers/Generating/Generating';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={Intro}/>
        <Route path="/step-two" component={ControlPanel}/>
        <Route path="/success" component={Success}/>
        <Route path="/generating" component={Generating}/>
    </Route>
);