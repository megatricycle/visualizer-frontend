import * as types from './appActionTypes';

export function addTracks(tracks) {
    return {
        type: types.ADD_TRACKS,
        tracks
    };
}

export function playMusic(track_id) {
    return {
        type: types.PLAY_MUSIC,
        track_id
    };
}

export function stopMusic() {
    return {
        type: types.STOP_MUSIC
    };
}

export function listLoaded() {
    return {
        type: types.LIST_LOADED
    };
}

export function listLoadStart() {
    return {
        type: types.LIST_LOAD_START
    };
}
