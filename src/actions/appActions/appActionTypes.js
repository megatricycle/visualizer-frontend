export const ADD_TRACKS = 'ADD_TRACKS';
export const PLAY_MUSIC = 'PLAY_MUSIC';
export const STOP_MUSIC = 'STOP_MUSIC';
export const LIST_LOADED = 'LIST_LOADED';
export const LIST_LOAD_START = 'LIST_LOAD_START';