import * as types from './controlPanelActionTypes';

export function changeBackground(bg_image) {
    return {
        type: types.CHANGE_BACKGROUND,
        bg_image
    };
}

export function changeVMode(v_mode) {
    return {
        type: types.CHANGE_V_MODE,
        v_mode
    };
}


export function toggleGenre(genre) {
    return {
        type: types.TOGGLE_GENRE,
        genre
    };
}

export function changeTitle(title) {
    return {
        type: types.CHANGE_TITLE,
        title
    };
}

export function changeArtist(artist) {
    return {
        type: types.CHANGE_ARTIST,
        artist
    };
}

export function selectTrack(track_id, file_name) {
    return {
        type: types.SELECT_TRACK,
        track_id,
        file_name
    };
}

export function reset() {
    return {
        type: types.RESET_STATE
    };
}