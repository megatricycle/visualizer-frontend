import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
require('./favicon.ico');
import injectTapEventPlugin from 'react-tap-event-plugin';
import './style.scss';

import routes from './routes';

injectTapEventPlugin();

const store = configureStore();

render(
    <Provider store={store}>
        <Router history={browserHistory} routes={routes}/>
    </Provider>, document.getElementById('app')
);